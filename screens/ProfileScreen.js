import React from 'react';
import { Text, View, ScrollView, StyleSheet } from 'react-native';
import { isNull, lowerCase, capitalize } from 'lodash';
import Badge from '../components/Badge';
import Separator from '../components/Helpers/separator';

export default class Profile extends React.Component {
  getRowTitle = item => capitalize(lowerCase(item));
  render() {
    const userInfo = this.props.navigation.getParam('info');
    const { avatar_url, name, login } = userInfo;
    const topicArr = [ 'company',
      'location',
      'followers',
      'following',
      'email',
      'bio',
      'public_repos' ];
    return (
      <ScrollView style={styles.container}>
        <Badge avatar={avatar_url} name={name} login={login} />
        {topicArr.map((item, index) =>
          isNull(userInfo[ item ]) ? <View key={index} /> : <View key={index}>
            <View style={styles.rowContainer}>
              <Text style={styles.rowTitle}>{this.getRowTitle(item)}</Text>
              <Text style={styles.rowContent}>{userInfo[ item ]}</Text>
            </View>
            <Separator />
          </View>
        )}
      </ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  rowContainer: {
    padding: 10,
  },
  rowTitle: {
    color: '#48BBEC',
    fontSize: 16
  },
  rowContent: {
    fontSize: 19
  }
});
