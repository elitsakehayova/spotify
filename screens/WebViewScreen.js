import React from 'react';
import { View, WebView, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

export default class Web extends React.Component {
  static propTypes = {
    url: PropTypes.string
  };

  render() {
    const url = this.props.navigation.getParam('url');
    return (
      <View style={styles.container}>
        <WebView source={{ uri: url }} />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6EF',
    flexDirection: 'column'
  }
})
