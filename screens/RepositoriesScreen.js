import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import Badge from '../components/Badge';
import PropTypes from 'prop-types';
import Placeholder from 'rn-placeholder';
import { getRepos } from '../services/api';
import RepositoriesInfo from '../components/RepositoriesInfo'

export default class Repositories extends React.Component {
  static propTypes = {
    repos: PropTypes.array
  };

  state = {
    repos: [],
  }

  componentDidMount = () => {
    const data = this.props.navigation.getParam('info');
    getRepos(data.login).then(res => {
      this.setState({ repos: res });
    })
  }

  openPage = url => {
    this.props.navigation.navigate('Web', { url });
  }

  render() {
    const info = this.props.navigation.getParam('info');
    const { avatar_url, name, login, public_repos } = info;
    const pub = Array(public_repos).fill(1);
    return (
      <ScrollView style={styles.container}>
        <Badge avatar={avatar_url} name={name} login={login} />
        {pub.map((item, index) => (
          <Placeholder.Paragraph
            key={index}
            lineNumber={3}
            textSize={20}
            lineSpacing={10}
            animate='fade'
            width="20%"
            firstLineWidth="40%"
            lastLineWidth="100%"
            onReady={this.state.repos.length ? true : false}
            style={styles.placeholder}>
            {this.state.repos[ index ] ? <RepositoriesInfo repo={this.state.repos[ index ]} goToPage={this.openPage} /> : <View />}
          </Placeholder.Paragraph>
        ))}
      </ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rowContainer: {
    flexDirection: 'column',
    flex: 1,
    padding: 10
  },
  name: {
    color: '#48BBEC',
    fontSize: 18,
    paddingBottom: 5,
  },
  stars: {
    color: '#48BBEC',
    fontSize: 14,
    paddingBottom: 5
  },
  description: {
    fontSize: 14,
    paddingBottom: 5
  },
  placeholder: {
    padding: 10
  }
});
