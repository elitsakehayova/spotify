import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image } from 'react-native';

const makeBackground = button => {
  let color = '#000';
  switch (button) {
    case 0:
      color = '#48BBEC'; break;
    case 1:
      color = '#E77AAE'; break;
    case 2:
      color = '#758BF4'; break;
  }
  return { backgroundColor: color }
};

export default class Dashboard extends React.Component {

  goToProfile = () => {
    const data = this.props.navigation.getParam('userInfo');
    this.props.navigation.navigate('Profile', { info: data });
  };

  goToRepo = () => {
    const data = this.props.navigation.getParam('userInfo');
    return this.props.navigation.navigate('Repositories', { info: data });
  };

  goToNotes = () => {
    console.log("Going to Notes")
  };

  render() {
    const param = this.props.navigation.getParam('userInfo');
    return (
      <View style={styles.container}>
        <Image source={{ uri: param.avatar_url }} style={styles.image} />
        <TouchableHighlight
          style={[ styles.button, makeBackground(0) ]}
          underlayColor='#88D45F'
          onPress={this.goToProfile}>
          <Text style={styles.buttonText}>View Profile</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[ styles.button, makeBackground(1) ]}
          underlayColor='#88D45F'
          onPress={this.goToRepo}>
          <Text style={styles.buttonText}>View Repo</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[ styles.button, makeBackground(2) ]}
          underlayColor='#88D4F5'
          onPress={this.goToNotes}>
          <Text style={styles.buttonText}>View Notes</Text>
        </TouchableHighlight>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 65,
  },
  image: {
    height: 350,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 24,
    color: 'white',
    alignSelf: 'center'
  }
});
