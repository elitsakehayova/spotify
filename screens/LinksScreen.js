import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableHighlight, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { getBio } from '../services/api';

export default class LinksScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  }

  state = {
    username: '',
    isLoading: false,
    error: false,
  }

  handleChange = event => this.setState({ username: event.nativeEvent.text });

  handleSubmit = () => {
    this.setState({ isLoading: 'true' });
    getBio(this.state.username).then(res => {
      this.setState({
        isLoading: false,
        error: false,
        username: ''
      }, () => {
        this.props.navigation.navigate('Dashboard', {
          userInfo: res
        });
      })
    }).catch(err => {
      err.response.status === 404 && this.setState({
        error: 'User not found',
        isLoading: false
      })
    }
    )
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.title}>Search for a Github User</Text>
        <TextInput
          autoFocus
          onSubmitEditing={this.handleSubmit}
          style={styles.searchInput}
          value={this.state.username}
          onChange={this.handleChange} />
        <TouchableHighlight
          style={styles.button}
          onPress={this.handleSubmit}
          underlayColor='white'>
          <Text style={styles.buttonText}> SEARCH</Text>
        </TouchableHighlight>
        <ActivityIndicator
          animating={this.state.isLoading}
          color='#111'
          size='large' />
        {this.state.error ? <Text>{this.state.error}</Text> : <View />}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    backgroundColor: '#48BBEC',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 25,
    color: '#fff'
  },
  searchInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    borderWidth: 1,
    fontSize: 23,
    color: 'white',
    borderColor: 'white',
    borderRadius: 8,
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginVertical: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});
