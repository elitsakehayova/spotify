import React from 'react';
import { View } from 'react-native';
import Counters from '../components/counters';
import Navbar from '../components/navbar';

export default class SettingsScreen extends React.Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter(c => c.id !== counterId);
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map(c => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  handleIncrement = counter => {
    const counters = [ ...this.state.counters ];
    const index = counters.indexOf(counter);
    counters[ index ] = { ...counter }
    counters[ index ].value++;
    this.setState({ counters });
  }

  render() {
    //console.log('App - Rendered');
    return (
      <View >
        <Navbar totalCounters={this.state.counters.filter(c => c.value > 0).length} />
        <Counters
          counters={this.state.counters}
          onReset={this.handleReset}
          onIncrement={this.handleIncrement}
          onDelete={this.handleDelete} />
      </View>
    );
  }
}

