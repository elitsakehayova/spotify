import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Placeholder from 'rn-placeholder';

export default class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Placeholder.Paragraph
          lineNumber={3}
          textSize={20}
          animate='shine'
          lineSpacing={10}
          width="20%"
          firstLineWidth="40%"
          lastLineWidth="100%"
          onReady={false}
        >
          <Text>Placeholder has finished :D</Text>
        </Placeholder.Paragraph>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingBottom: 15
  }
});
