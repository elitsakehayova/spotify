import axios from 'axios';

export const getBio = username => {
  username = username.toLowerCase().trim();
  const url = `https://api.github.com/users/${username}`;
  return axios.get(url).then(res => res.data);
}

export const getRepos = username => {
  username = username.toLowerCase().trim();
  const url = `https://api.github.com/users/${username}/repos`;
  return axios.get(url).then(res => res.data);
}
