import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class Navbar extends React.Component {
  render() {
    return (
      <View width='100%' backgroundColor='#eaebed' flexDirection='row' padding={15}>
        <Text style={styles.text}>Navbar </Text>
        <Text style={styles.num}>{this.props.totalCounters}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'left',
    marginRight: 10
  },
  num: {
    borderRadius: 50,
    backgroundColor: '#999a9b',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 25,
    paddingVertical: 5,
    paddingHorizontal: 10
  }
});
