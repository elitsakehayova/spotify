import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import Counter from '../components/counter';

export default class Counters extends React.Component {
  render() {
    //const { onReset, counters, onDelete, onIncrement } = this.props; 
    // console.log('Components - Rendered')
    return (
      <View marginTop={10} >
        <TouchableOpacity style={styles.button} onPress={this.props.onReset} >
          <Text style={styles.textButton}>Reset</Text>
        </TouchableOpacity>
        {this.props.counters.map(counter =>
          <Counter
            key={counter.id}
            onDelete={this.props.onDelete}
            onIncrement={this.props.onIncrement}
            counter={counter} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 25
  },
  button: {
    padding: 5,
    backgroundColor: 'blue',
    borderRadius: 3,
    marginBottom: 10,
    justifyContent: 'center',
    width: 75,
    marginLeft: 120
  },
})
