import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class Counter extends React.Component {
  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? 'Zero' : value;
  };

  render() {
    //console.log('Conuter - Rendered');
    return (
      <View flexDirection='row' width='100%' justifyContent='center' marginBottom={10}>
        <Text style={styles.text}>{this.formatCount()} </Text>
        <TouchableOpacity style={styles.button} onPress={() => this.props.onIncrement(this.props.counter)}>
          <Text style={styles.textButton}>Increment</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonDelete} onPress={() => this.props.onDelete(this.props.counter.id)} >
          <Text style={styles.textButton}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    borderRadius: 5,
    backgroundColor: 'yellow',
    paddingVertical: 10,
    fontWeight: 'bold',
    fontSize: 25,
    width: 75,
    textAlign: 'center',
    marginRight: 10,
  },
  button: {
    padding: 5,
    backgroundColor: '#88898c',
    borderRadius: 3,
    marginRight: 10,
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 25
  },
  buttonDelete: {
    padding: 5,
    backgroundColor: '#88898c',
    borderRadius: 3,
    backgroundColor: 'red'
  }
});
