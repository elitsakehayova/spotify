import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Separator from '../components/Helpers/separator';

export default RepositoriesInfo = ({ repo, goToPage }) => (
  <React.Fragment>
    <View style={styles.rowContainer} >
      <TouchableHighlight
        onPress={() => goToPage(repo.html_url)}
        underlayColor='transparent'>
        <Text style={styles.name}>{repo.name}</Text>
      </TouchableHighlight>
      <Text style={styles.stars}>Stars: {repo.stargazers_count}</Text>
      {repo.description ? <Text style={styles.description}>{repo.description}</Text> : <View />}
    </View>
    <Separator />
  </React.Fragment>
);

const styles = StyleSheet.create({
  name: {
    color: '#48BBEC',
    fontSize: 18,
    paddingBottom: 5,
  },
  stars: {
    color: '#48BBEC',
    fontSize: 14,
    paddingBottom: 5
  },
  description: {
    fontSize: 14,
    paddingBottom: 5
  },
  rowContainer: {
    flexDirection: 'column',
    flex: 1,
    padding: 10
  },
})
